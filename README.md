# create_bugzilla_5_0_4_user

create_bugzilla_5_0_4_user is a python 3.8 script to automatically create
bugzilla users from an exchange mailing list. If a user already exist in
bugzilla, the account will be skipt.

## Installation

Clone repository

git clone https://gitlab.com/uath4u/create_bugzilla_5-0-4_users.git

Install dependencies with pip.

pip3 install requirements.txt

## Configuration

Put your exchange and bugzilla credentials and server names to the script.

Search for

- EXCHANGE USERNAME
- EXCHANGE PASSWORD
- EXCHANGE SERVER NAME
- EXCHANGE USER EMAIL

and replace it with your credentials, user email or server address.

Search for:

- MAILING LIST NAME

and replace it with your mailing list name to read users from.

Search for:

- BUGZILLA SERVER ADDRESS
- BUGZILLA USER NAME
- BUGZILLA PASSWORD

and replace it with your credentials or server address.

Search for:

- DOMAIN NAME

and replace it witch the domain for standard user.


## Usage

Start the script and all users in the mailing list will be created, if they does
not already exist in bugzilla.
