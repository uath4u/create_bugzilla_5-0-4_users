#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import warnings
import json
import requests

from urllib3.exceptions import InsecureRequestWarning
from pyPasswordGenerator import password
from exchangelib import Account, Credentials, Configuration


class CreateBugzillaUser:
    """Main Class to bring useraccounts from echange mailing list to bugzilla 5.0.4

        Attributes

        exchange_account_connection:    exchangelib.Account-Object
        bugzilla_token:                 str containing bugzilla token to verify to bugzilla as api_user


        Modules

        process_all_users:              reads email-addresses from VL and
                                        applies every email-address to self._create_user_availability().
        _create_user_availability:      Search for the current email-address by calling self.search_user() and if the
                                        user is found skips the email. If email is not found create_user() is called.
                                        User_full_name is created by calling aggregate_user_full_name() and set as the
                                        parameter for create_user.
        _create_user:                   First a random password is generated and then the user iss created by calling
                                        Bugzilla Web Api 5.04
        _search_user:                   Searches for email in Bugzilla by calling Bugzilla Web Api 5.0.4 and returns the
                                        result to caller-function.
        _aggregate_user_full_name:      aggregates user_full_name from email-address.
        _get_bugzilla_token:            get new bugzilla token to verify to bugzilla
        _get_exchange_connection:       Establish a connection to exchange server
        """

    def __init__(self):
        """Instantiating class CreateBugzillaUser"""

        self.exchange_account_connection = self._get_exchange_connection()
        self.bugzilla_token = self._get_bugzilla_token()

    def _get_bugzilla_token(self):
        """get new bugzilla token for a user to verify to bugzilla.

        get_bugzilla_token(self) -> None
        """

        logging.info('Getting new bugzilla token')

        return requests.get(f'INSERT BUGZILLA SERVER ADDRESS/rest.cgi/login?login={INSERT BUGZILLA USERNAME}'
                            f'&password={INSERT BUBZILLE PASSWORD}', verify=False).json()['token']

    def _get_exchange_connection(self):
        """Establish the connection to exchange server

        _get_exchange_connectin() -> exchangelib.Account
        """

        credentials = Credentials('INSERT EXCHANGE USERNAME', 'INSERT EXCHANGE PASSWORD')
        config = Configuration(credentials=credentials, server='INSERT EXCHANGE SERVER ADDRESS')

        return Account(f'INSERT EXCHANGE USER EMAIL', credentials=credentials, config=config)

    def process_all_users(self):
        """reads all nested email-addresses from mailing list and applies every
        email-address found to self._create_unavailable_user().

        create_unavailaable_users(self) -> None
        """

        logging.info('Create users from VL Alle MitarbeiterInnen File')

        try:

            for mailbox_in_dl in self.exchange_account_connection.protocol.expand_dl('INSERT MAILING LIST NAME'):
                self._create_user_availability(mailbox_in_dl.email_address)

        except Exception as exc:

            logging.error(f'Reading Exchange raised an error\n\n\t{exc}')

    def _create_user_availability(self, user_mail_to_create='user.anlegen.test@gINSERT DOMAIN NAME'):
        """Search for the current email-address by calling self._search_user() and if the user is found skips the
        email-address. If email is not found create_user() is called. User_full_name is created by calling
        aggregate_user_full_name() and set as the parameter for create_user.

        create_unavailable_user(self, user_mail_to_create -> String) -> None
        """

        if self._search_user(user_mail_to_create):

            logging.info(f'Can\'t create user {user_mail_to_create} because the account already exists.')

        else:

            self._create_user(user_mail_to_create, self._aggregate_user_full_name(user_mail_to_create))

    def _create_user(self, user_to_create='user.anlegen.test@INSERT DOMAIN NAME', user_full_name='User Anlegen Test'):
        """First a random password is generated and then the user iss created by calliing Bugzilla Web Api 5.04

        create_user(user_to_create -> String(email-address) -> None
        """

        try:

            logging.info(f'Creating user {user_to_create}')

            password_set = '!#%+=?01223456789ABCDEFGHJKLMNPRSTUVWXYZabcdefghijkmnopqrstuvwxyz'

            user_data_json = json.dumps({
                "email": user_to_create,
                "full_name": user_full_name,
                "password": password.generator(set=password_set, length=16, upper=3, symbol=4, digits=5)
            })

            call_headers = {'Content-Type': 'application/json'}

            response = requests.post(f'INSERT BUGZILLA SERVER ADDRESS/rest.cgi/user?Bugzilla_token='
                                     f'{self.bugzilla_token}', headers=call_headers, verify=False,
                                     data=user_data_json)

            if response.status_code == 201:

                logging.info(f'User {user_to_create} was created successfully')

            else:

                logging.error(f'Can\'t create user {user_to_create} because of {response.content}')

        except Exception as exc:

            logging.error(f'Can\'t create user {user_to_create} because of {exc}')

            exit(1)

    def _search_user(self, user_mail_to_search='user.anlegen.test@INSERT DOMAIN Forename'):
        """Searching for email-address by calliing Bugzilla Web Api 5.04 If one ore more users found for current serarch
        email-address, a list of users is returned. If no user is found None will be returned.

        search_user(user_to_search -> String(email-address) -> List(users in bugzilla) or None"""

        logging.info(f'Searching for user {user_mail_to_search}')

        try:

            search_response = requests.get(f'INSERT BUGZILLA SERVER ADDRESS/rest.cgi/user/{user_mail_to_search}'
                                           f'?Bugzilla_token={self.bugzilla_token}', verify=False)

            if search_response.status_code == 200:

                return json.loads(search_response.text)['users']

            else:

                return None

        except Exception as exc:

            logging.error(f'Couldn\'t search for user {user_mail_to_search} because of Error\n\n{exc}')

            exit(1)

    @staticmethod
    def _aggregate_user_full_name(user_email='user.anlegen.test@INSERT DOMAIN NAME'):
        """Aggregates full username in format 'Forename Lastename' from email-address.

        aggregate_user_full_name(user_email -> String(email-address)) -> String(Forename Lastname)"""

        try:

            username = ''

            for username_part in user_email[:user_email.find('@')].split('.'):

                if username == '':

                    username = username_part[0].upper() + username_part[1:]

                else:

                    username = username + ' ' + username_part[0].upper() + username_part[1:]

            return username

        except Exception as exc:

            print(exc)


if __name__ == '__main__':

    # Prepare Workspace

    warnings.simplefilter('ignore', InsecureRequestWarning)

    # Script is doing its work

    logging.info('Starting script')

    instance = CreateBugzillaUser()
    instance.process_all_users()

    logging.info('Script finished')

    exit(0)
